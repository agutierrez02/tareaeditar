import { Injectable } from '@angular/core';
import { productos, proveedor } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
      nombre: "Antonio",
      cedula: 1252865,
      direccion: "San Jose",
      numero: 85411666,
      correo: "antoio@gmail.com",
      codigo: 1
    },
  ];
  private productos: productos[] = [
    {
      proveedor: this.proveedore,
      precio: 1000,
      candidad: 2,
      codigo: 2447,
      nombre: "Bolas de futbol",
      peso: 11,
      descripcion: "Bolas de futbol de todos los tamaños"
    },
    {
      proveedor: this.proveedore,
      precio: 2000,
      candidad: 4,
      codigo: 6818,
      nombre: "Camisetas",
      peso: 5,
      descripcion: "Camisetas de deporte"
    },
    {
      proveedor: this.proveedore,
      precio: 4000,
      candidad: 1,
      codigo: 1525,
      nombre: "Shorts",
      peso: 5,
      descripcion: "Shorts para correr"
    }
  ];
  constructor() { }
  getAll() {
    return [...this.productos];
  }
  getProduct(productId: number) {
    return {
      ...this.productos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };
  }
  deleteProduct(productId: number) {
    this.productos = this.productos.filter(
      product => {
        return product.codigo !== productId;
      }
    );
  }
  addProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pdescripcion: string
  ) {
    const product: productos = {
      proveedor: this.proveedore,
      precio: pprecio,
      candidad: pcantidad,
      codigo: pcodigo,
      nombre: pnombre,
      peso: ppeso,
      descripcion: pdescripcion
    }
    this.productos.push(product);
  }
  editProduct(
    pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pdescripcion: string
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);

    this.productos[index].codigo = pcodigo;
    this.productos[index].candidad = pcantidad;
    this.productos[index].nombre = pnombre;
    this.productos[index].peso = ppeso;
    this.productos[index].descripcion = pdescripcion;
    this.productos[index].precio = pprecio;

    console.log(this.productos);
  }
}
